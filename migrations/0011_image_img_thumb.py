# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-17 18:40
from __future__ import unicode_literals

from django.db import migrations, models
from ..models import Entry, Image


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0010_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='img_thumb',
            field=models.ImageField(blank=True, editable=False, null=True, upload_to='thumbs', verbose_name='Image Thumbnail'),
        ),
    ]

    # for e in Entry.objects.all():
    #     print('e.image = ', e.image)
    #     image = Image()
    #     image.post = e
    #     image.image = e.image
    #     image.img_thumb = e.img_thumb
    #     image.save()
