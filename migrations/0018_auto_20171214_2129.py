# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-12-14 21:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0017_auto_20171214_2115'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClimateStation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('latlon', models.CharField(blank=True, max_length=30, null=True, verbose_name='Lat/Lon [decimal degrees]')),
                ('period_of_record', models.CharField(blank=True, max_length=30, null=True, verbose_name='Period of Record')),
                ('distance_to_target', models.CharField(blank=True, max_length=30, null=True, verbose_name='Distance to Target [km]')),
                ('station_name', models.CharField(blank=True, max_length=30, null=True, verbose_name='Station Name')),
                ('station_ID', models.CharField(blank=True, max_length=30, null=True, verbose_name='Station ID')),
                ('new_file_name', models.CharField(blank=True, max_length=30, null=True, verbose_name='File Name (for csv writing)')),
            ],
        ),
        migrations.AlterField(
            model_name='stationsearchtarget',
            name='search_radius',
            field=models.DecimalField(choices=[(1.0, '1km'), (5.0, '5km'), (10.0, '10km')], decimal_places=1, default='1km', max_digits=3, verbose_name='Search Radius [km]'),
        ),
    ]
