# Generated by Django 2.0 on 2018-01-03 13:22

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dkht', '0005_auto_20180102_1947'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stationsearchtarget',
            name='lat',
            field=models.DecimalField(decimal_places=4, max_digits=9, validators=[django.core.validators.MinValueValidator(-80.0), django.core.validators.MaxValueValidator(84)], verbose_name='Target Latitude [decimal degrees]'),
        ),
    ]
